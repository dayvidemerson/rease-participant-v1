'use strict';
var modules = [
  'ngCookies',
  'ngResource',
  'ui.router',
  'ui.mask',
  'oc.lazyLoad',
  'ngAuthentication'
];

angular
  .module('rease-participante', modules)
  .constant('api', 'https://api.rease.com.br');