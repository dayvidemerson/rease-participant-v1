'use strict'
angular.module('rease-participante')
  .factory('PasswordReset', PasswordReset)
 
PasswordReset.$inject = ['api', '$http']

function PasswordReset(api, $http) {
  var url = api+'/core/password/reset/'

  PasswordReset.sendEmail = function(email) {
    return $http.post(url, {'email': email})
  }

  PasswordReset.reset = function(token, password, confirm_password) {
    return $http.put(url+token+'/', {
      'password': password,
      'confirm_password': confirm_password
    })
  }

  return PasswordReset
}