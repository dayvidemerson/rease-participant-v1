'use strict'
angular.module('rease-participante')
  .factory('Minicurso', Minicurso)
 
Minicurso.$inject = ['api', '$http', '$filter']

function Minicurso(api, $http, $filter) {
  var url = api+'/participante/minicursos/'

  var Minicurso = function(data) {
    this.pk = data.pk
    this.nome = data.nome
    this.carga_horaria = data.carga_horaria
    this.edicao = data.edicao
  }

  Minicurso.all = function(pk) {
    return $http.get(url, {
      params: {
        "edicao": pk
      }
    })
  }

  Minicurso.get = function(pk) {
    return $http.get(url+pk)
  }

  return Minicurso
}