'use strict'
angular.module('rease-participante')
  .factory('TurmaAtividade', TurmaAtividade)
 
TurmaAtividade.$inject = ['api', '$http']

function TurmaAtividade(api, $http) {
  var url = api+'/participante/turmas/atividade/'

  var TurmaAtividade = function(data) {
    this.pk = data.pk
    this.atividade = data.atividade
    this.descricao = data.descricao
    this.vagas = data.vagas_disponiveis
  }

  TurmaAtividade.all = function(pk) {
    return $http.get(url, {
      params: {
        "atividade__edicao": pk,
      }
    })
  }

  TurmaAtividade.get = function(pk) {
    return $http.get(url+pk)
  }

  return TurmaAtividade
}