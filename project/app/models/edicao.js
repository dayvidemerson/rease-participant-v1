'use strict'
angular.module('rease-participante')
    .factory('Edicao', Edicao)

Edicao.$inject = ['api', '$http', 'Time']

function Edicao(api, $http, Time) {
    var url = api + '/participante/edicoes/'

    var Edicao = function(data) {
        this.pk = data.pk;
        this.nome = data.nome
        this.edicao = data.edicao
        this.cep = data.cep
        this.logradouro = data.logradouro
        this.numero = data.numero
        this.bairro = data.bairro
        this.cidade = data.cidade
        this.estado = data.estado
        this.complemento = data.complemento
        this.logomarca = data.logomarca
        this.evento = data.evento
        this.status = data.status
        this.inicio = Time.date(data.inicio)
        this.termino = Time.date(data.termino)
        this.inicio_inscricao = Time.date(data.inicio_inscricao)
        this.termino_inscricao = Time.date(data.termino_inscricao)
        this.areas = data.areas
    }

    Edicao.all = function() {
        return $http.get(url)
    }

    return Edicao
}