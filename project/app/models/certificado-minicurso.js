'use strict'
angular.module('rease-participante')
	.factory('CertificadoMinicurso', CertificadoMinicurso)

CertificadoMinicurso.$inject = ['api', '$http']

function CertificadoMinicurso(api, $http) {
  var url = api+'/participante/certificados/minicurso/'

  var CertificadoMinicurso = function(data) {
    this.pk = data.pk
    this.token = data.token
    this.nome_certificado = data.nome_certificado
    this.disponivel = data.disponivel
    this.evento = data.evento
    this.turma = data.turma
  }

  CertificadoMinicurso.prototype = {
    post: function() {
      return $http.post(url, this.json())
    },
    updateName: function() {
      return $http.put(api+'/participante/certificados/nome/'+this.pk+'/', this.json())
    },
    json: function() {
      return JSON.stringify(this)
    }
  }

  CertificadoMinicurso.all = function() {
    return $http.get(url)
  }

  return CertificadoMinicurso
}