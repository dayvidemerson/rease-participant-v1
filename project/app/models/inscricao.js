'use strict';
angular.module('rease-participante')
    .factory('Inscricao', Inscricao);

Inscricao.$inject = ['api', '$http'];

function Inscricao(api, $http) {
    var url = api + '/participante/inscricoes/';

    var Inscricao = function(data) {
        this.pk = data.pk;
        this.tipo = data.tipo;
        this.preco = data.preco;
        this.status = data.status;
        this.minicursos = data.minicursos;
        this.atividades = data.atividades;
        this.palestras = data.palestras;
        this.documentos_enviados = data.documentos_enviados;
        this.documentos_necessarios = data.documentos_necessarios;
        this.atividades_gerais = data.atividades_gerais;
        this.certificados_palestras = data.certificados_palestras;
        this.edicao = data.edicao;
        this.certificados_minicursos = data.certificados_minicursos;
        this.certificados_atividades = data.certificados_atividades;
        this.certificado_evento = data.certificado_evento;
        this.conta = data.conta;
    };

    Inscricao.prototype = {
        post: function() {
            return $http.post(url, this.json());
        },
        json: function() {
            return JSON.stringify(this);
        }
    };

    Inscricao.all = function() {
        return $http.get(url)
    };

    return Inscricao;
}