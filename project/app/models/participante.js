(function() {
    'use strict';
    angular.module('rease-participante')
        .factory('Participante', Participante);

    Participante.$inject = ['api', '$http', '$filter', 'Time'];

    function Participante(api, $http, $filter, Time) {
        var url = api + '/evento/participante/';

        var Participante = function(data) {
            this.email = data.email;
            this.first_name = data.first_name;
            this.last_name = data.last_name;
            this.data_nascimento = Time.date_str(data.data_nascimento);
            this.cpf = data.cpf;
            this.password = data.password;
            this.confirm_password = data.confirm_password;
        }

        Participante.prototype = {
            post: function() {
                return $http.post(url, this.json());
            },
            put: function() {
                return $http.put(url, this.json());
            },
            json: function() {
                var json = this;
                json.data_nascimento = Time.date(json.data_nascimento);
                json.data_nascimento = $filter('date')(this.data_nascimento, 'yyyy-MM-dd');
                return JSON.stringify(json);
            }
        }

        Participante.get = function() {
            return $http.get(url);
        }

        return Participante;
    }
})();