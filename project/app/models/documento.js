'use strict'
angular.module('rease-participante')
  .factory('Documento', Documento)

Documento.$inject = ['api', '$http']

function Documento(api, $http) {
  var url = api+'/participante/documentos/'

  var Documento = function(data) {
    this.inscricao = data.inscricao
    this.tipo = data.tipo
    this.nome_tipo = data.nome_tipo
    this.documento = data.documento
  }

  Documento.prototype = {
    post: function() {
      return $http.post(url, this.json())
    },
    json: function() {
      return JSON.stringify(this)
    }
  }

  return Documento
}