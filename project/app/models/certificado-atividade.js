'use strict'
angular.module('rease-participante')
	.factory('CertificadoAtividade', CertificadoAtividade)

CertificadoAtividade.$inject = ['api', '$http']

function CertificadoAtividade(api, $http) {
  var url = api+'/participante/certificados/atividade/'

  var CertificadoAtividade = function(data) {
    this.pk = data.pk
    this.token = data.token
    this.nome_certificado = data.nome_certificado
    this.disponivel = data.disponivel
    this.evento = data.evento
    this.turma = data.turma
  }

  CertificadoAtividade.prototype = {
    post: function() {
      return $http.post(url, this.json())
    },
    updateName: function() {
      return $http.put(api+'/participante/certificados/nome/'+this.pk+'/', this.json())
    },
    json: function() {
      return JSON.stringify(this)
    }
  }

  CertificadoAtividade.all = function() {
    return $http.get(url)
  }

  return CertificadoAtividade
}