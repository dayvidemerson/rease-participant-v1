'use strict'
angular.module('rease-participante')
  .factory('Grupo', Grupo)
 
Grupo.$inject = ['api', '$http']

function Grupo(api, $http) {
  var url = api+'/participante/grupos/'

  var Grupo = function(data) {
    this.pk = data.pk
    this.nome = data.nome
    this.minicursos = data.minicursos
    this.atividades = data.atividades
  }

  Grupo.all = function(pk) {
    return $http.get(url, {
      params: {
        'edicao': pk
      }
    })
  }

  return Grupo
}