'use strict'
angular.module('rease-participante')
  .factory('Palestra', Palestra)
 
Palestra.$inject = ['api', '$http', 'Time']

function Palestra(api, $http, Time) {
  var url = api+'/participante/palestras/'

  var Palestra = function(data) {
    this.pk = data.pk
    this.nome = data.nome
    this.hora = Time.time(data.hora)
    this.data = Time.date(data.data)
    this.carga_horaria = data.carga_horaria
    this.profissional = data.profissional
    this.nome_profissional = data.nome_profissional
    this.edicao = data.edicao
  }

  Palestra.all = function(pk) {
    return $http.get(url, {
      params: {
        "edicao": pk
      }
    })
  }

  Palestra.get = function(pk) {
    return $http.get(url+pk)
  }

  return Palestra
}