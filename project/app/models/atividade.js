'use strict'
angular.module('rease-participante')
  .factory('Atividade', Atividade)
 
Atividade.$inject = ['api', '$http']

function Atividade(api, $http) {
  var url = api+'/participante/atividades/'

  var Atividade = function(data) {
    this.pk = data.pk
    this.nome = data.nome
    this.carga_horaria = data.carga_horaria
    this.edicao = data.edicao
  }

  Atividade.all = function(pk) {
    return $http.get(url, {
      params: {
        "edicao": pk
      }
    })
  }

  Atividade.get = function(pk) {
    return $http.get(url+pk)
  }

  return Atividade
}