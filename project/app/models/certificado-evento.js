'use strict'
angular.module('rease-participante')
    .factory('CertificadoEvento', CertificadoEvento)

CertificadoEvento.$inject = ['api', '$http']

function CertificadoEvento(api, $http) {
    var url = api + '/participante/certificados/evento/'

    var CertificadoEvento = function(data) {
        this.pk = data.pk
        this.token = data.token
        this.nome_certificado = data.nome_certificado
        this.disponivel = data.disponivel
        this.descricao = data.descricao
        this.evento = data.evento
    }

    CertificadoEvento.prototype = {
        updateName: function() {
            return $http.put(api + '/participante/certificados/nome/' + this.pk + '/', this.json())
        },
        json: function() {
            return JSON.stringify(this)
        }
    }

    CertificadoEvento.all = function() {
        return $http.get(url)
    }

    return CertificadoEvento
}