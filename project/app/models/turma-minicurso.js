'use strict'
angular.module('rease-participante')
  .factory('TurmaMinicurso', TurmaMinicurso)
 
TurmaMinicurso.$inject = ['api', '$http']

function TurmaMinicurso(api, $http) {
  var url = api+'/participante/turmas/minicurso/'

  var TurmaMinicurso = function(data) {
    this.pk = data.pk
    this.minicurso = data.minicurso
    this.vagas = data.vagas_disponiveis
    this.profissional = data.profissional
  }

  TurmaMinicurso.all = function(pk) {
    return $http.get(url, {
      params: {
        "minicurso__edicao": pk,
      }
    })
  }

  TurmaMinicurso.get = function(pk) {
    return $http.get(url+pk)
  }

  return TurmaMinicurso
}