'use strict';
angular.module('rease-participante')
    .factory('AtividadeGeral', AtividadeGeral);

AtividadeGeral.$inject = ['api', '$http', 'Time'];

function AtividadeGeral(api, $http, Time) {
    var url = api + '/participante/atividades/gerais/';

    var AtividadeGeral = function(data) {
        this.pk = data.pk;
        this.descricao = data.descricao;
        this.carga_horaria = data.carga_horaria;
        this.data = Time.date(data.data);
        this.inicio = Time.time(data.inicio);
        this.termino = Time.time(data.termino);
        this.edicao = data.edicao;
    };

    AtividadeGeral.all = function(pk) {
        return $http.get(url, {
            params: {
                "edicao": pk
            }
        });
    };

    AtividadeGeral.get = function(pk) {
        return $http.get(url + pk);
    };

    return AtividadeGeral;
}