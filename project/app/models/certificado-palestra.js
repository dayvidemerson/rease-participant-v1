'use strict'
angular.module('rease-participante')
    .factory('CertificadoPalestra', CertificadoPalestra)

CertificadoPalestra.$inject = ['api', '$http']

function CertificadoPalestra(api, $http) {
    var url = api + '/participante/certificados/palestra/'

    var CertificadoPalestra = function(data) {
        this.pk = data.pk
        this.token = data.token
        this.nome_certificado = data.nome_certificado
        this.disponivel = data.disponivel
        this.evento = data.evento
        this.palestra = data.palestra
    }

    CertificadoPalestra.prototype = {
        post: function() {
            return $http.post(url, this.json())
        },
        updateName: function() {
            return $http.put(api + '/participante/certificados/nome/' + this.pk + '/', this.json())
        },
        json: function() {
            return JSON.stringify(this)
        }
    }

    CertificadoPalestra.all = function() {
        return $http.get(url)
    }

    return CertificadoPalestra
}