'use strict'
angular.module('rease-participante')
  .factory('TipoInscricao', TipoInscricao)
 
TipoInscricao.$inject = ['api', '$http']

function TipoInscricao(api, $http) {
  var url = api+'/participante/tipos/inscricao/'

  var TipoInscricao = function(data) {
    this.pk = data.pk
    this.nome = data.nome
    this.palestra_obrigatoria = data.palestra_obrigatoria
    this.preco_minicurso = data.preco_minicurso
    this.preco_palestra = data.preco_palestra
    this.preco_atividade = data.preco_atividade
  }

  TipoInscricao.prototype = {
    preco: function(inscricao) {
      var preco = 0.0
      preco += this.preco_minicurso*inscricao.certificados_minicursos.length
      preco += this.preco_atividade*inscricao.certificados_atividades.length
      if (this.palestra_obrigatoria || inscricao.certificados_palestras) {
        preco += this.preco_palestra
      }
      return preco
    },
  }

  TipoInscricao.all = function(pk) {
    return $http.get(url, {
      params: {
        "edicao": pk
      }
    })
  }

  TipoInscricao.get = function(pk) {
    return $http.get(url+pk)
  }

  return TipoInscricao
}