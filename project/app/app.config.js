'use strict'
angular.module('rease-participante')
  .config(config)

config.$inject = ['$stateProvider', '$httpProvider', '$locationProvider', '$urlRouterProvider', '$ocLazyLoadProvider']

function config($stateProvider, $httpProvider, $locationProvider, $urlRouterProvider, $ocLazyLoadProvider) {
  $ocLazyLoadProvider.config({
    modules: [{
      name: 'navbar',
      files: ['app/directives/js/navbar.js']
    },
    {
      name: 'alert',
      files: ['app/directives/js/alert.js']
    },
    {
      name: 'panel',
      files: ['app/directives/js/panel.js']
    },
    {
      name: 'modal',
      files: ['app/directives/js/modal.js']
    },
    {
      name: 'file-reader',
      files: ['app/directives/js/file-reader.js']
    }]
  })
  $httpProvider.defaults.xsrfCookieName = 'csrftoken'
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'
  $locationProvider.html5Mode(true)
  $urlRouterProvider.otherwise('/')
  $stateProvider
    .state('sendPasswordResetEmail', {
      url: '/password/reset/',
      views: {
        '': {
          controller: 'SendPasswordResetEmailController',
          controllerAs: 'vm',
          templateUrl: '/app/views/send-password-reset-email.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/models/password-reset.js',
              'app/controllers/send-password-reset-email.js'
            ]
          })
        }]
      }
    })
    .state('passwordReset', {
      url: '/password/reset/{token:string}',
      views: {
        '': {
          controller: 'PasswordResetController',
          controllerAs: 'vm',
          templateUrl: '/app/views/password-reset.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/models/password-reset.js',
              'app/controllers/password-reset.js'
            ]
          })
        }]
      }
    })
    .state('login', {
      url: '/',
      views: {
        '': {
          controller: 'LoginController',
          controllerAs: 'vm',
          templateUrl: 'app/views/login.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/controllers/login.js'
            ]
          })
        }]
      }
    })
    .state('logout', {
      url: '/logout/',
      views: {
        '': {
          controller: 'LogoutController',
          controllerAs: 'vm',
          template: ''
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load([
            'app/controllers/logout.js'
          ])
        }]
      }
    })
    .state('register', {
      url: '/register/',
      views: {
        '': {
          controller: 'ParticipanteController',
          controllerAs: 'vm',
          templateUrl: '/app/views/register.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/models/participante.js',
              'app/controllers/participante.js'
            ]
          })
        }]
      }
    })
    .state('update', {
      url: '/update/',
      views: {
        '': {
          controller: 'ParticipanteUpdateController',
          controllerAs: 'vm',
          templateUrl: '/app/views/update.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/models/participante.js',
              'app/controllers/participante-update.js'
            ]
          })
        }]
      }
    })
    .state('home', {
      url: '/home/',
      views: {
        '': {
          controller: 'HomeController',
          controllerAs: 'vm',
          templateUrl: '/app/views/home.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/models/edicao.js',
              'app/models/palestra.js',
              'app/models/inscricao.js',
              'app/models/documento.js',
              'app/controllers/home.js'
            ]
          })
        }]
      }
    })
    .state('listInscricoes', {
      url: '/inscricoes/',
      views: {
        '': {
          controller: 'InscricaoListController',
          controllerAs: 'vm',
          templateUrl: '/app/views/inscricao-list.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/models/inscricao.js',
              'app/models/documento.js',
              'app/controllers/inscricao-list.js'
            ]
          })
        }]
      }
    })
    .state('createInscricoes', {
      url: '/inscricoes/{edicao:int}',
      views: {
        '': {
          controller: 'InscricaoCreateController',
          controllerAs: 'vm',
          templateUrl: '/app/views/inscricao-form.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/models/edicao.js',
              'app/models/grupo.js',
              'app/models/palestra.js',
              'app/models/atividade-geral.js',
              'app/models/tipo-inscricao.js',
              'app/models/inscricao.js',
              'app/models/documento.js',
              'app/controllers/inscricao-create.js'
            ]
          })
        }]
      }
    })
    .state('certificados', {
      url: '/certificados/',
      views: {
        '': {
          controller: 'CertificadoController',
          controllerAs: 'vm',
          templateUrl: '/app/views/certificado.html'
        }
      },
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            serie: true,
            cache: true,
            files: [
              'app/models/certificado-minicurso.js',
              'app/models/certificado-atividade.js',
              'app/models/certificado-palestra.js',
              'app/models/certificado-evento.js',
              'app/controllers/certificado.js'
            ]
          })
        }]
      }
    })
}