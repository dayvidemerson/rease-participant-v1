requirejs.config({
  baseUrl: 'app/',
  paths: {
    'custom': '/assets/js/custom',
    'jquery': '/bower_components/jquery/dist/jquery.min',
    'bootstrap': '/bower_components/bootstrap/dist/js/bootstrap.min',
    'angular': '/bower_components/angular/angular.min',
    'angular-cookies': '/bower_components/angular-cookies/angular-cookies.min',
    'angular-resource': '/bower_components/angular-resource/angular-resource.min',
    'ui.router': '/bower_components/angular-ui-router/release/angular-ui-router.min',
    'ui.mask': '/bower_components/angular-ui-mask/dist/mask.min',
    'ocLazyLoad': '/bower_components/oclazyload/dist/ocLazyLoad.min'
  },
  shim: {
    'bootstrap': ['jquery'],
    'custom': ['jquery'],
    'angular-cookies': ['angular'],
    'angular-resource': ['angular'],
    'ui.router': ['angular'],
    'ui.mask': ['angular'],
    'ocLazyLoad': ['angular'],
    'services/authentication': ['angular'],
    'app.module': ['angular-cookies', 'angular-resource', 'ui.router', 'ui.mask', 'ocLazyLoad', 'services/authentication'],
    'services/certificado': ['app.module'],
    'services/extra': ['app.module'],
    'services/flash': ['app.module'],
    'services/time': ['app.module'],
    'app.config': ['app.module', 'services/certificado', 'services/extra', 'services/flash', 'services/time'],
    'app.run': ['app.config']
  }
});

requirejs(['app.run', 'bootstrap', 'custom'], function() {
  angular.bootstrap(document, ['rease-participante']);
});