'use strict'
angular.module('rease-participante')
  .factory('CertificadoService', CertificadoService)
 
CertificadoService.$inject = ['api', '$http']

function CertificadoService(api, $http) {
  var service = {}

  service.getCertificado = getCertificado

  return service

  function getCertificado(token) {
    return $http.get(api+'/participante/certificados/'+token+'/')
  }
}