'use strict'
angular.module('rease-participante')
  .factory('Extra', Extra)
 
Extra.$inject = []

function Extra() {
  var service = {}

  service.pks = pks
  service.getForPk = getForPk

  return service

  function pks(list) {
    var response = []
    angular.forEach(list, function(value, key) {
      response.push(value.pk)
    })
    return response
  }

  function getForPk(list, pk) {
    var object = null
    angular.forEach(list, function(value, key) {
      if (value.pk == pk) {
        object = value
      }
    })
    return object
  }

  return Extra
}