'use strict'
angular.module('rease-participante')
	.factory('Time', Time)
 
Time.$inject = ['$filter']

function Time($filter) {
  var service = {}

  service.date = date
  service.date_str = date_str
  service.time = time

  return service

  function date(data) {
    if (data && !(data instanceof Date)) {
      if (data.includes("-")) {
        var ano = data.slice(0, 4)
        var mes = data.slice(5, 7)
        var dia = data.slice(8, 10)
      } else {
        var ano = data.slice(4, 8)
        var mes = data.slice(2, 4)
        var dia = data.slice(0, 2)
      }
      return new Date(parseInt(ano), parseInt(mes)-1, parseInt(dia))
    }
    return data
  }

  function date_str(data) {
    if (data) {
      if (data.includes("-")) {
        var ano = data.slice(0, 4)
        var mes = data.slice(5, 7)
        var dia = data.slice(8, 10)
      } else if (typeof data == 'string') {
        return data
      } else {
        return $filter('date')(this.data, 'ddMMyyyy')
      }
      return dia.concat(mes, ano);
    }
    return data
  }

  function time(time) {
    if (time && !(time instanceof data)) {
      var inicio = time.slice(0, 2)
      var minuto = time.slice(3, 5)
      return new data(1970, 0, 1, inicio, minuto, 0)
    }
    return time
  }

  return Time
}