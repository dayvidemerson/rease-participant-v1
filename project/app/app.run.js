'use strict'
angular.module('rease-participante')
  .run(run)

run.$inject = ['$http', '$state', '$cookies', '$rootScope', 'AuthenticationService']

function run($http, $state, $cookies, $rootScope, AuthenticationService) {
  $rootScope.loading = true
  $rootScope.menu = [
    {
      label: 'Perfil',
      state: 'update',
      icon: 'user'
    },
    {
      label: 'Eventos',
      state: 'home',
      icon: 'list'
    },
    {
      label: 'Inscrições',
      state: 'listInscricoes',
      icon: 'calendar'
    },
    {
      label: 'Certificados',
      state: 'certificados',
      icon: 'certificate'
    }
  ]
  var token = $cookies.get('token')
  if (token) {
    $rootScope.token = token
    $http.defaults.headers.common.Authorization = 'Token ' + token
  }

  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    $rootScope.loading = false
  })

  $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    if (toState.name == 'register' || toState.name == 'login' || toState.name == 'passwordReset'  || toState.name == 'sendPasswordResetEmail') {
      $rootScope.body_class = 'login-bg'
      $rootScope.content_class = 'container'
    } else {
      $rootScope.body_class = ''
      $rootScope.content_class = 'container'
    }
    if (!AuthenticationService.isLogged() && toState.name != 'register' && toState.name != 'passwordReset' && toState.name != 'sendPasswordResetEmail') {
      $state.go('login')
    } else if (AuthenticationService.isLogged() && (toState.name == 'login' || toState.name == 'register' || toState.name == 'passwordReset'  || toState.name == 'sendPasswordResetEmail')) {
      $state.go('home')
    }
    $rootScope.loading = true
  })
}