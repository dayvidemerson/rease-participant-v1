'use strict'
angular.module('rease-participante')
	.directive('navbar', navbar)

function navbar() {
	var ddo = {};
	ddo.restric = 'E';
	ddo.transclude = true
	ddo.templateUrl = 'app/directives/html/navbar.html'
	return ddo;
}