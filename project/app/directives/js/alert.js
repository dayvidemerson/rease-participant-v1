'use strict'
angular.module('rease-participante')
  .directive('alert', alert)

function alert() {
  var ddo = {};
  ddo.restric = 'E';
  ddo.scope = {
    tipo: '@'
  }
  ddo.transclude = true;
  ddo.templateUrl = 'app/directives/html/alert.html';
  return ddo;
}