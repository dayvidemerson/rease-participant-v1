'use strict'
angular.module('rease-participante')
  .controller('InscricaoListController', InscricaoListController)

InscricaoListController.$inject = ['$window', 'Inscricao', 'Documento', 'flash']

function InscricaoListController($window, Inscricao, Documento, flash) {
  var vm = this
  vm.flash = flash
  vm.message = []
  vm.data = {}
  vm.list_loading = false
  vm.inscricoes = []

  vm.submit = function(inscricao) {
    vm.data.inscricao = inscricao.pk
    vm.data.tipo = vm.data.tipo.pk
    var instance = new Documento(vm.data)
    if (vm.form.$valid) {          
      instance.post()
        .then(function(response) {
          Inscricao.all()
            .then(function(response) {
              vm.inscricoes = []
              length = response.data.length
              for (var i = 0; i < length; i++) {
                vm.inscricoes.push(new Inscricao(response.data[i]))
              }
            }, function(reject) {
              vm.message.push({
                texto: reject.data.detail,
                tipo: 'danger'
              })
            })
          vm.message.push({
            texto: 'Documento enviado com sucesso!',
            tipo: 'success'
          })
        }, function(reject) {
          vm.error = reject.data
        })
    }
  }

  Inscricao.all()
    .then(function(response) {
      length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.inscricoes.push(new Inscricao(response.data[i]))
      }
      vm.list_loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })
}