'use strict'
angular.module('rease-participante')
  .controller('PasswordResetController', PasswordResetController)

PasswordResetController.$inject = ['PasswordReset', '$state', '$stateParams', 'flash']

function PasswordResetController(PasswordReset, $state, $stateParams, flash) {
  var vm = this
  vm.flash = flash
  vm.message = []
  vm.error = {}
  vm.data = {}
  vm.confirmButton = 'Salvar'
  vm.submit = function(){
    vm.confirmButton = 'Carregando'
    if (vm.form.$valid) {
      PasswordReset.reset($stateParams.token, vm.data.password, vm.data.confirm_password)
        .then(function(response) {
          flash.setMessage(response.data.message)
          $state.go('login')
        }, function(reject) {
          vm.message.push(reject.data.error)
          vm.error = reject.data
          vm.confirmButton = 'Salvar'
        })
    }
  }
}