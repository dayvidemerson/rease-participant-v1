'use strict';
angular.module('rease-participante')
	.controller('LoginController', LoginController);

LoginController.$inject = ['AuthenticationService', '$state', 'flash'];

function LoginController(AuthenticationService, $state, flash) {
	var vm = this;
	vm.flash = flash;
	vm.message = [];
	vm.error = {};
	vm.data = {};
	vm.confirmButton = 'Entrar';
	vm.submit = function(){
		vm.confirmButton = 'Carregando';
		if (vm.form.$valid) {
			AuthenticationService.get(vm.data)
				.then(function(response) {
					AuthenticationService.set(response.data);
					$state.go('home');
				}, function(reject) {
					vm.message = reject.data.non_field_errors;
					vm.error = reject.data;
					vm.confirmButton = 'Entrar';
				});
		}
	}
}