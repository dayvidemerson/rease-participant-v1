'use strict'
angular.module('rease-participante')
	.controller('ParticipanteController', ParticipanteController)

ParticipanteController.$inject = ['Participante', '$state', 'flash', '$rootScope']

function ParticipanteController(Participante, $state, flash, $rootScope) {
	var vm = this
	vm.flash = flash
	vm.data = {}
	vm.error = {}
	vm.confirmButton = 'Confirmar'

	// Form

	vm.submit = function() {
		vm.confirmButton = 'Carregando'
		var instance = new Participante(vm.data)
		if (vm.form.$valid) {
			instance.post()
				.then(function(response) {
					vm.flash.setMessage('Usuário registrado com sucesso!')
					$state.go('login')
				}, function(reject) {
					vm.error = reject.data
				})
		}
		vm.confirmButton = 'Confirmar'
	}
}