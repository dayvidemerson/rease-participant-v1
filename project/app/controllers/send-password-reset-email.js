'use strict'
angular.module('rease-participante')
  .controller('SendPasswordResetEmailController', SendPasswordResetEmailController)

SendPasswordResetEmailController.$inject = ['PasswordReset', '$state', 'flash']

function SendPasswordResetEmailController(PasswordReset, $state, flash) {
  var vm = this
  vm.flash = flash
  vm.message = []
  vm.error = {}
  vm.data = {}
  vm.confirmButton = 'Enviar'
  vm.submit = function(){
    vm.confirmButton = 'Carregando'
    if (vm.form.$valid) {
      PasswordReset.sendEmail(vm.data.email)
        .then(function(response) {
          flash.setMessage(response.data.message)
          $state.go('login')
        }, function(reject) {
          vm.message.push(reject.data.error)
          vm.error = reject.data
          vm.confirmButton = 'Enviar'
        })
    }
  }
}