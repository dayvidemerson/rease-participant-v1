'use strict';
angular.module('rease-participante')
    .controller('InscricaoCreateController', InscricaoCreateController);

InscricaoCreateController.$inject = ['Inscricao', 'TipoInscricao', 'Grupo', 'Palestra', 'AtividadeGeral', 'Extra', '$state', '$stateParams', '$filter', 'flash'];

function InscricaoCreateController(Inscricao, TipoInscricao, Grupo, Palestra, AtividadeGeral, Extra, $state, $stateParams, $filter, flash) {
    var vm = this;
    vm.flash = flash;
    vm.certificados = [];
    vm.data = {
        certificados_minicursos: [],
        certificados_atividades: [],
        certificados_palestras: false,
        certificado_evento: false
    };
    vm.certificado_obrigatorio = true;
    vm.grupos = [];
    vm.tipos = [];
    vm.palestras = [];
    vm.atividades_gerais = [];
    vm.preco = 0;
    vm.error = {};
    vm.edicao = $stateParams.edicao;
    vm.confirmButton = 'Confirmar';

    TipoInscricao.all(vm.edicao)
        .then(function(response) {
            length = response.data.length;
            for (var i = 0; i < length; i++) {
                vm.tipos.push(new TipoInscricao(response.data[i]));
            }
        }, function(reject) {
            vm.error = reject.data.detail;
        })

    Grupo.all(vm.edicao)
        .then(function(response) {
            length = response.data.length;
            for (var i = 0; i < length; i++) {
                vm.grupos.push(response.data[i]);
                vm.certificados[i] = '';
            }
        }, function(reject) {
            vm.error = reject.data.detail;
        })

    Palestra.all($stateParams.edicao)
        .then(function(response) {
            vm.palestras = response.data;
            if (vm.palestras.length) {
                vm.data.certificados_palestras = true;
            }
        }, function(reject) {
            vm.error = reject.data.detail;
        });

    AtividadeGeral.all($stateParams.edicao)
        .then(function (response) {
            vm.atividades_gerais = response.data;
            vm.data['certificado_evento'] = true;
        })

    vm.cleanGrupo = function(grupo) {
        var length = vm.data.certificados_minicursos.length;
        var index = -1;
        for (var i = 0; i < length; i++) {
            var minicurso = Extra.getForPk(grupo.minicursos, vm.data.certificados_minicursos[i]);
            if (minicurso) {
                vm.data.certificados_minicursos.splice(i, 1);
                break;
            }
        }
        length = vm.data.certificados_atividades.length;
        for (var i = 0; i < length; i++) {
            var atividade = Extra.getForPk(grupo.atividades, vm.data.certificados_atividades[i]);
            if (atividade) {
                vm.data.certificados_atividades.splice(i, 1);
                break;
            }
        }
    }

    vm.selectedTurmasMinicursos = function(grupo, value) {
        vm.cleanGrupo(grupo);
        vm.data.certificados_minicursos.push(value);
    }

    vm.selectedTurmasAtividades = function(grupo, value) {
        vm.cleanGrupo(grupo);
        vm.data.certificados_atividades.push(value);
    }

    vm.estimatedPrice = function() {
        var tipo = Extra.getForPk(vm.tipos, vm.data.tipo);
        vm.certificado_obrigatorio = tipo.palestra_obrigatoria;
        vm.preco = tipo.preco(vm.data);
    }

    // Form

    vm.submit = function() {
        vm.confirmButton = 'Carregando';
        if (vm.data.tipo.palestra_obrigatoria) {
            vm.data.certificados_palestras = true;
        }
        var instance = new Inscricao(vm.data);
        if (vm.form.$valid) {
            instance.post()
                .then(function(response) {
                    vm.flash.setMessage('Inscricao registrada com sucesso!');
                    $state.go('listInscricoes');
                }, function(reject) {
                    vm.error = reject.data;
                    console.log(reject.data);
                    vm.confirmButton = 'Confirmar';
                })
        }
    }

    vm.getDescricao = function(turma) {
        window.open(turma.descricao, 'Certificado')
    }

}