'use strict';
angular.module('rease-participante')
  .controller('ParticipanteUpdateController', ParticipanteUpdateController);

ParticipanteUpdateController.$inject = ['Participante', '$state', 'flash', '$rootScope'];

function ParticipanteUpdateController(Participante, $state, flash, $rootScope) {
  var vm = this;
  vm.flash = flash;
  vm.data = {};
  vm.error = {};
  vm.confirmButton = 'Confirmar';

  Participante.get()
    .then(function(response) {
      vm.data = new Participante(response.data);
    });

  // Form

  vm.submit = function() {
    vm.confirmButton = 'Carregando';
    var instance = new Participante(vm.data);
    console.log(instance.data_nascimento)
    if (vm.form.$valid) {
      instance.put()
        .then(function(response) {
          vm.flash.setMessage('Participante atualizado com sucesso!');
          $state.go('home');
        }, function(reject) {
          vm.error = reject.data;
        });
    }
    vm.confirmButton = 'Confirmar';
  }
}