
'use strict'
angular.module('rease-participante')
	.controller('LogoutController', LogoutController)

LogoutController.$inject = ['AuthenticationService', '$state']

function LogoutController(AuthenticationService, $state) {
	AuthenticationService.destroy()
	$state.go('login')
}