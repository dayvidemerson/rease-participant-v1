'use strict'
angular.module('rease-participante')
  .controller('CertificadoController', CertificadoController)

CertificadoController.$inject = ['CertificadoMinicurso', 'CertificadoEvento', 'CertificadoAtividade', 'CertificadoPalestra', 'CertificadoService']

function CertificadoController(CertificadoMinicurso, CertificadoEvento, CertificadoAtividade, CertificadoPalestra, CertificadoService) {
  var vm = this
  vm.message = []
  vm.data = {}
  vm.list_loading = false
  vm.certificados = false
  vm.certificados_minicurso = []
  vm.certificados_atividade = []
  vm.certificados_palestra = []
  vm.certificados_evento = []
  vm.loadingCertificado = false

  CertificadoMinicurso.all()
    .then(function(response) {
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.certificados_minicurso.push(new CertificadoMinicurso(response.data[i]))
      }
      if (length) {
        vm.certificados = true
      }
      vm.list_loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  CertificadoAtividade.all()
    .then(function(response) {
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.certificados_atividade.push(new CertificadoAtividade(response.data[i]))
      }
      if (length) {
        vm.certificados = true
      }
      vm.list_loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  CertificadoPalestra.all()
    .then(function(response) {
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.certificados_palestra.push(new CertificadoPalestra(response.data[i]))
      }
      if (length) {
        vm.certificados = true
      }
      vm.list_loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })

  CertificadoEvento.all()
    .then(function(response) {
      console.log(response.data);
      var length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.certificados_evento.push(new CertificadoEvento(response.data[i]))
      }
      if (length) {
        vm.certificados = true
      }
      vm.list_loading = true
    }, function(reject) {
      console.log(reject.data);
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })


  vm.getCertificado = function(token) {
    vm.loadingCertificado = true
    CertificadoService.getCertificado(token)
      .then(function(response) {
        vm.loadingCertificado = false
        window.open(response.data)
      }, function(reject) {
        vm.loadingCertificado = false
        vm.message.push({
          text: reject.data,
          tipo: "danger"
        })
      })
  }

  vm.submitNomeCertificado = function(certificado) {
    if (vm.form.$valid) {
      certificado.nome_certificado = vm.data.nome_certificado
      certificado.updateName()
        .then(function(response) {
          vm.message.push({
            text: response.data,
            tipo: 'success'
          })
        }, function(reject) {
          vm.message.push({
            text: reject.data,
            tipo: 'danger'
          })
        })
    }
  }
}