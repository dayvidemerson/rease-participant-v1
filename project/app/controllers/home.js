'use strict'
angular.module('rease-participante')
  .controller('HomeController', HomeController)

HomeController.$inject = ['Edicao', 'Inscricao', 'Documento', 'flash']

function HomeController(Edicao, Inscricao, Documento, flash) {
  var vm = this
  vm.flash = flash
  vm.list_loading = false
  vm.message = []
  vm.edicoes = []

  Edicao.all()
    .then(function(response) {
      length = response.data.length
      for (var i = 0; i < length; i++) {
        vm.edicoes.push(new Edicao(response.data[i]))
      }
      vm.list_loading = true
    }, function(reject) {
      vm.message.push({
        texto: reject.data.detail,
        tipo: 'danger'
      })
    })
}